CREATE TABLE user(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	login TEXT,
	address TEXT
);

CREATE TABLE role(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	label TEXT
);

CREATE TABLE user_has_role(
	user_id INTEGER,
	role_id INTEGER,
	FOREIGN KEY(user_id) REFERENCES user(id),
	FOREIGN KEY(role_id) REFERENCES role(id)
);

CREATE TABLE plant(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name TEXT,
	to_guard INTEGER,
	owner_id INTEGER,
	FOREIGN KEY(owner_id) REFERENCES user(id)
);

CREATE TABLE user_guard_plant(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	guard_id INTEGER,
	plant_id INTEGER,
	date INTEGER,
	FOREIGN KEY(guard_id) REFERENCES user(id),
	FOREIGN KEY(plant_id) REFERENCES plant(id)
);

CREATE TABLE user_comment_plant(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	botanist_id INTEGER,
	plant_id INTEGER,
	comment TEXT,
	date INTEGER,
	FOREIGN KEY(botanist_id) REFERENCES user(id),
	FOREIGN KEY(plant_id) REFERENCES plant(id)
);

CREATE TABLE picture(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	plant_id INTEGER,
	url TEXT,
	FOREIGN KEY(plant_id) REFERENCES plant(id)
);