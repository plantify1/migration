#!/bin/bash

while getopts 'p:' OPTION; do
  case "$OPTION" in
    p)
	  password=${OPTARG}
      ;;
  esac
done
shift "$(($OPTIND -1))"

if [ -z "$password" ]
then
	echo "Vous devez renseigner le mot de passe root"
else
	mysql -u 'root' --password="$password" --execute='DROP DATABASE Plantify; CREATE DATABASE Plantify;'
    sqlite3mysql -f Plantify.db -d Plantify -u 'root' --mysql-password "$password"
    mysql -u 'root' --password="$password" Plantify < edit-mysql-database.sql
fi

