#modification de user
ALTER TABLE user
MODIFY COLUMN login VARCHAR(55),
MODIFY COLUMN address VARCHAR(200);

# modification de role
ALTER TABLE role
MODIFY COLUMN label VARCHAR(55);

# modification de plant : to_guard devient un BOOLEAN, i.e un TINYINT(1) 
ALTER TABLE plant
MODIFY COLUMN name VARCHAR(55),
MODIFY COLUMN to_guard BOOLEAN;

# modification de user_guard_plant : conversion de date de INT en TIMESTAMP
ALTER TABLE user_guard_plant ADD date_updated TIMESTAMP;
UPDATE user_guard_plant SET date_updated = FROM_UNIXTIME(date);
ALTER TABLE user_guard_plant DROP COLUMN date;
ALTER TABLE user_guard_plant RENAME COLUMN date_updated TO date;

# modification de user_comment_plant : conversion de date de INT en TIMESTAMP
ALTER TABLE user_comment_plant ADD date_updated TIMESTAMP;
UPDATE user_comment_plant SET date_updated = FROM_UNIXTIME(date);
ALTER TABLE user_comment_plant DROP COLUMN date;
ALTER TABLE user_comment_plant RENAME COLUMN date_updated TO date;

# modification de picture : longueur max d'un URL = 2048 caractères
ALTER TABLE picture
MODIFY COLUMN url VARCHAR(2048);