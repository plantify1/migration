FROM ubuntu/mysql

WORKDIR /app

COPY . .
COPY create-mysql-db.sql /docker-entrypoint-initdb.d

RUN apt-get update

RUN apt-get install python3-pip -y
RUN pip install sqlite3-to-mysql

RUN apt-get install sqlite3 -y

RUN chmod +x create-sqlite-db.sh
RUN chmod +x migrate-sqlite-to-mysql.sh

RUN ./create-sqlite-db.sh
