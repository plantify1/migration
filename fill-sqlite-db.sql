INSERT INTO user (login, address) 
VALUES
	("Bernard", "13 rue du foin"),
	("Jacqueline", "Partout et nulle part à la fois"),
	("E.T.", "Pas vraiment de cette planète"),
	("Damien", "12 place Sainte-Anne");

INSERT INTO role (label)
VALUES
	("admin"),
	("owner"),
	("guard"),
	("botanist");

INSERT INTO user_has_role (user_id, role_id)
VALUES
	(1, 1),
	(1, 3),
	(2, 1),
	(2, 2),
	(2, 3),
	(2, 4),
	(3, 2),
	(4, 1),
	(4, 2),
	(4, 3),
	(4, 4);

INSERT INTO plant (name, to_guard, owner_id)
VALUES
	("Titine", 1, 1),
	("Jojo", 0, 1),
	("Brouette", 0, 1),
	("Bella", 1, 1),
	("Caramel", 1, 1),
	("Florilège", 0, 2),
	("Emeraude", 1, 2),
	("Magazine", 0,3),
	("Princier", 1, 3),
	("Belle-du-Ventoux", 1, 3),
	("Haricot", 1, 3),
	("Médor", 0, 3),
	("Pergolas", 1, 3),
	("DJSet", 1, 3),
	("Tupperware", 1, 4),
	("Idiomatique", 0, 4),
	("Fabrice", 0, 4);

-- date = nombre de secondes depuis epoch.

INSERT INTO user_guard_plant (guard_id, plant_id, date)
VALUES
	(1, 2, 1689285600),
	(1, 4, 1695333600),
	(1, 3, 1692396000),
	(1, 7, 1701212400),
	(1, 12, 1710457200),
	(2, 5, 1710457200),
	(2, 7, 1701385200),
	(2, 17, 1701385200),
	(2, 15, 1690668000),
	(4, 1, 1690754400),
	(4, 13, 1693087200);

INSERT INTO user_comment_plant (botanist_id, plant_id, comment, date)
VALUES
	(2, 2, "Qu'est-ce que c'est que cette plante ? Je n'en ai jamais vu de telle.", 1672560000),
	(4, 2, "Moi non plus, c'est bizarre ces fleurs jaunes. Aïe aïe aïe.", 1672646400),
	(2, 3, "C'est un galynacé, ou alors je m'y connais pas.", 1672732800),
	(4, 5, "Mazette comme c'est chouchou.", 1672819200),
	(2, 6, "C'est pas une plante ça XD PTDR.", 1673769600),
	(2, 6, "Ah si c'est peut-être une plante en fait.", 1673856000),
	(2, 6, "Ah non.", 1673942400),
	(4, 7, "OMG ! Mais c'est de toute beauté.", 1673164800),
	(4, 8, "La plus belle, la divine, la magnifique ! Je vous conseille quand même de l'arroser", 1674201600),
	(4, 10, "Arroser, replanter, arroser.", 1672800000),
	(2, 10, "Il n'y a plus rien à faire malheureusement.", 1672828800),
	(2, 11, "C'est un abricot, ou un poireau, je m'y perds.", 1673059200),
	(2, 13, "J'ai un serpent dans ma botte.", 1673001600),
	(4, 13, "Quelqu'un a empoisonné le point d'eau.", 1672713600),
	(2, 14, "Cette plante manque de tout. Il en faut peu pour être heureux, mais quand même.", 1672656000),
	(4, 15, "Stanislas Wawrinka au service pour le gain du deuxième set.", 1672771200),
	(4, 16, "Une plante en forme, ça fait plaisir à voir.", 1672713600),
	(2, 16, "Elle est bien entretenue, c'est vrai.", 1673059200),
	(4, 16, "C'est ce que je viens de dire.", 1672598400),
	(4, 17, "Circulez y a rien à voir héhé !", 1672857600);


INSERT INTO picture (url, plant_id)
VALUES
	("https://fastly.picsum.photos/id/758/200/300.jpg?hmac=lQtDVVjQGklGEIBCA-5yXBI3L8zkkeGObzmCi-rUFKo", 1),
	("https://fastly.picsum.photos/id/1002/200/300.jpg?hmac=QAnT71VGihaxEf_iyet9i7yb3JvYTzeojsx-djd3Aos", 1),
	("https://fastly.picsum.photos/id/135/200/300.jpg?hmac=d3sTOCUkxdC1OKCgh9wTPjck-gMWATyVHFvflla5vLI", 1),
	("https://fastly.picsum.photos/id/70/200/300.jpg?hmac=8-6v4fVxk6exesGT53s01yaJuediQIreacSHqZY3mV4", 1),
	("https://fastly.picsum.photos/id/1070/200/300.jpg?hmac=dJNTYlLwT_0RupxbJNbw5Wj-q2cCTB4Xh-GqWRofIIc", 2),
	("https://fastly.picsum.photos/id/450/200/300.jpg?hmac=EAnz3Z3i5qXfaz54l0aegp_-5oN4HTwiZG828ZGD7GM", 2),
	("https://fastly.picsum.photos/id/520/200/300.jpg?hmac=wYOWhYQGp5efB1HNroao-yTysVtEt5osptkdHJIsc0g", 3),
	("https://fastly.picsum.photos/id/861/200/300.jpg?hmac=kssNLkcAZTJQKpPCSrGodykV8A6CStZxL7dHvtsVUD0", 4),
	("https://fastly.picsum.photos/id/703/200/300.jpg?hmac=NKQDtZdJOFjdXxez-vNSj9VLDpR9jIewJzUhoOJoZVI", 4),
	("https://fastly.picsum.photos/id/127/200/300.jpg?hmac=H0aErkmw8FxF1Tp7uFj4cV-aVMxDDjOVKTwGwS6REXw", 5),
	("https://fastly.picsum.photos/id/860/200/300.jpg?hmac=IABW-3mXBCuVP5aHuuuC7WpE3w1TpQZS9PZYr4mf7Gk", 5),
	("https://fastly.picsum.photos/id/210/200/300.jpg?hmac=xq_AloJ8pIOvSilFZdFGDfUQZb3gN-nApbDPSecpqa4", 5),
	("https://fastly.picsum.photos/id/1069/200/300.jpg?hmac=z7ef02jy_-2I0_UTVob-AN6AWxP7-4bTJmZZnnLKMgk", 6),
	("https://fastly.picsum.photos/id/575/200/300.jpg?hmac=sopd2rAqqxeAtI5YKmESfglb3av7FRnaTdo3woj1uEM", 6),
	("https://fastly.picsum.photos/id/36/200/300.jpg?hmac=yeKKPp3h_shxmrZgoKPc6ix1TOSmkj8Rs9FZVpFzljA", 6),
	("https://fastly.picsum.photos/id/1078/200/300.jpg?hmac=cIIdxycLQ-pOopDajDdPoOPwl9nnMld2aG58k8IgyX4", 6),
	("https://fastly.picsum.photos/id/371/200/300.jpg?hmac=CZPdOAGtsgzhjapSpcZbjc4cFkTu5gWl9PFxBRY369c", 6),
	("https://fastly.picsum.photos/id/240/200/300.jpg?hmac=oqwZqcYrZ2nqhtDKiob6qVc3u_vuKLh89nVzKs_jnNg", 7),
	("https://fastly.picsum.photos/id/693/200/300.jpg?hmac=mVvEbAr0g-bdhrVxrz7IorqUfEy95C2hPkIT9Vm3nn8", 8),
	("https://fastly.picsum.photos/id/834/200/300.jpg?hmac=9hu4aro5r8PEFwzVlhizygx4urxyeGGjgyMRXUgKOsE", 9),
	("https://fastly.picsum.photos/id/1027/200/300.jpg?hmac=WCxdERZ7sgk4jhwpfIZT0M48pctaaDcidOi3dKSHJYY", 10),
	("https://fastly.picsum.photos/id/410/200/300.jpg?hmac=c8g8PTUISEdRZEqZGLtB3eOCOq7t5A5rJY1vU8dp_Ds", 10),
	("https://fastly.picsum.photos/id/780/200/300.jpg?hmac=Zmxf0t2fpCbfZrR5NAXA_IKAP_8P6fYe9P440jUTWag", 11),
	("https://fastly.picsum.photos/id/1011/200/300.jpg?hmac=3OASTCcuMs99-ZFi2rl7Rh9DuaNJXZytGmDyOsRm7Hw", 11),
	("https://fastly.picsum.photos/id/880/200/300.jpg?hmac=dShSJOHRB--zjrqofJOm33xe4Cylybn00N77ewnaS2g", 11),
	("https://fastly.picsum.photos/id/735/200/300.jpg?hmac=1a236E3f0SNOHOLEh3dxu5_WIFvWaNKYBSZXBWpi6xE", 12),
	("https://fastly.picsum.photos/id/802/200/300.jpg?hmac=q6ItUSh1lSpO66uCg28JvcSG6TC_XXIOgCwifpzTD9M", 12),
	("https://fastly.picsum.photos/id/63/200/300.jpg?hmac=Zhw62KKdLbsw5yRcx9gVDEQq4kzPwjZUrJAJUIryu6k", 12),
	("https://fastly.picsum.photos/id/46/200/300.jpg?hmac=hO6W_-hkJCRf3aWSzs0SkaWPFlMhZsixxrObPW_sFaY", 12),
	("https://fastly.picsum.photos/id/27/200/300.jpg?hmac=cxfyms4Ce9ExYqZqSCKEppGQpmi8rRNNaf46Lwr5iqA", 12),
	("https://fastly.picsum.photos/id/541/200/300.jpg?hmac=nhG-hlD63wW6srZpMlMH73GwqdwqiMD5VDrLV7TQJ08", 13),
	("https://fastly.picsum.photos/id/75/200/300.jpg?hmac=sjSIzdmDj0dZefwBIN61pwl3azxymhEGh9owb8ZEgxg", 13),
	("https://fastly.picsum.photos/id/867/200/300.jpg?hmac=l9Sb50fRDDS2EGp1lHYsoxOdsO88E-eRygNU36pTQ-s", 13),
	("https://fastly.picsum.photos/id/365/200/300.jpg?hmac=n_4DxqK0o938eabBZRnEywWtPwgF2MKoTfnRmJ7vlKQ", 13),
	("https://fastly.picsum.photos/id/372/200/300.jpg?hmac=Ng2Fl0_1eMGEpJhcZtvsqTvSOF7vxR0fxsPI6hPm_nk", 13),
	("https://fastly.picsum.photos/id/627/200/300.jpg?hmac=C6cEU431ILuZjftVFQ1RsBlFYS52ym9f2KZPSOfH-R4", 14),
	("https://fastly.picsum.photos/id/318/200/300.jpg?hmac=WEC_ft7NGxXgRDHWhj1tz7_gmAOrnI9d5IiS98juw8I", 14),
	("https://fastly.picsum.photos/id/270/200/300.jpg?hmac=To24fO6lmJwfKPyA9r3T_t07xLNZz3q_weS3ISynEGg", 14),
	("https://fastly.picsum.photos/id/855/200/300.jpg?hmac=2aBMcUWlYEynKymdtTjCpwCpl2v_ELuWkkmOeWbjqa0", 14),
	("https://fastly.picsum.photos/id/1070/200/300.jpg?hmac=dJNTYlLwT_0RupxbJNbw5Wj-q2cCTB4Xh-GqWRofIIc", 14),
	("https://fastly.picsum.photos/id/129/200/300.jpg?hmac=orJWwWZR-Y_APTSxTwuQsz8j4ROmKGMTOuZEVPyY-3M", 15),
	("https://fastly.picsum.photos/id/277/200/300.jpg?hmac=8wIsJ0ZOFtBSrTpSE0M37XZG2dXMzDTL2_dgppOaP0Y", 15),
	("https://fastly.picsum.photos/id/230/200/300.jpg?hmac=pyhlpgJN2oBeEzhJbnJYrCsLoJM6MKd_NUQGIQhVx5k", 16),
	("https://fastly.picsum.photos/id/290/200/300.jpg?hmac=kjRyFwJ6i5kuROjzxcs6QbXbBr8EptbH5AuVxtMxhQ0", 16),
	("https://fastly.picsum.photos/id/277/200/300.jpg?hmac=8wIsJ0ZOFtBSrTpSE0M37XZG2dXMzDTL2_dgppOaP0Y", 16),
	("https://fastly.picsum.photos/id/971/200/300.jpg?hmac=13ePfSXGucrfsPCzPjUZtNe5jYo83FTet0dk-Lk2Q4E", 16),
	("https://fastly.picsum.photos/id/916/200/300.jpg?hmac=AlGE1xEsSBVvJKbHoDnjf9v5TRINh8LNMN6xwzQieO0", 17),
	("https://fastly.picsum.photos/id/551/200/300.jpg?hmac=pXJCWIikY_BiqwhtawBb8x1jxclDny0522ZprZVTJiU", 17);

