# Migration de SQLite vers MySQL

## Application

Le but de ces quelques scripts est de montrer comment s'effectue la migration de la base de données SQLite de notre application Plantify vers une base de données MySQL.

## Installation

L'intégralité des scripts est intégrée dans une image Docker. Pour la télécharger :

```
docker pull plantify/migration
```

Cette image a une base _ubuntu_, avec _mysql_, _sqlite3_ et _Python_ installés. 

Par ailleurs, elle contient une base de données en SQLite qui reprend le schéma de notre application, généré par le fichier _create-sqlite-db.sql_, un jeu de données pour remplir cette base (_fill-sqlite-db.sql_) et une base de données MySQL vide.

Pour créer un conteneur se basant sur cette image :

```
docker run -d --name <my-container> -e MYSQL_ROOT_PASSWORD=<my-password> plantify/migration
```

La variable d'environnement MYSQL_ROOT_PASSWORD renseigne le mot de passe permettant d'accéder à la base de données MySQL une fois le conteneur exécuté.

Pour exécuter le conteneur et accéder à une interface en ligne de commande :

```
docker exec -it <my-container> /bin/bash
```

## Utilisation

Une fois dans l'interface, vous pouvez consulter la base de données SQLite Plantify :

```
sqlite3 Plantify.db
```

Une fois dans la CLI sqlite, vous pouvez voir en détail le contenu des tables de la base, en faisant des requête SQL et en entrant des commandes propres à SQLite (plus d'infos [ici](https://www.tutorialspoint.com/sqlite/sqlite_commands.htm)).

Pour vous connecter à la base de données MySQL :

```
mysql -u root --password=<my-password> Plantify
```

Cette base de données est vide à l'origine, mais on pourra en consulter les données une fois la migration effectuée à l'aide de commandes SQL (plus d'infos [ici](https://www.w3schools.com/mysql/mysql_sql.asp)).

Pour effectuer la migration de données, un script (détaillé ci-dessous) peut être lancé :

```
./migrate-sqlite-to-mysql.sh -p <my-password>
```

Si vous souhaitez recréer la base de données SQLite :

```
./create-sqlite-db.sh
```

## Principe de la migration

La difficulté autour de la migration vient du fait que les types de données en SQLite ne sont pas les mêmes qu'en MySQL. Dans cet exemple, le script _migrate-sqlite-db.sh_ gère la migration. Il se décompose en 2 étapes.

### Migration de la structure

Pour migrer la structure de données, on utilise une bibliothèque Python [_sqlite3-to-mysql_](https://pypi.org/project/sqlite3-to-mysql/), munie d'une interface en ligne de commandes.

Une fois cette première étape passée, la structure de la donnée est conservée : on a les mêmes tables, composées des mêmes entrées, munies des mêmes clés primaire et secondaires, et des mêmes propriétés (l'auto-incrémentation des colonnes _id_ des différentes tables par exemple).

Cependant, les variables ont toutes des types par défaut qui ne correspondent pas finement à la donnée manipulée. Ainsi, les données _TEXT_ en SQLite sont automatiquement converties en données _TEXT_ en MySQL, là où un _VARCHAR_ par exemple peut suffire.

### Mise à jour des types de données

La deuxième étape consiste à adapter les types de données MySQL. Pour cela, on lance une suite d'opérations SQL qui modifie les colonnes des différentes tables (_edit-mysql-database.sql_).

Par exemple, les dates sont enregistrées dans la base SQLite sous la forme d'_INTEGER_ qui représente le nombre de secondes depuis _epoch_. Nous convertissons cette donnée en _TIMESTAMP_, en MySQL (format qui n'existe pas en SQLite), de la sorte :

```sql
ALTER TABLE user_comment_plant ADD date_updated TIMESTAMP;
UPDATE user_comment_plant SET date_updated = FROM_UNIXTIME(date);
ALTER TABLE user_comment_plant DROP COLUMN date;
ALTER TABLE user_comment_plant RENAME COLUMN date_updated TO date;
```

On crée une nouvelle colonne dans la table. La fonction FROM_UNIXTIME de MySQL fait la conversion, puis on supprime la colonne _date_ qui n'est plus utile pour la base de données MySQL.

